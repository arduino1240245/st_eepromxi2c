
//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6) y un 24LC256

#include <Wire.h>
#define chipAddress 80    //Porque respeta el 101 que indica las hojas de dato en A0, A1, A2

#define LED PC13
#define SDA PB7
#define SCL PB6


/*  SDA ==> PB7
 *  SCL ==> PB6
 *  
 *  tAAA# ==> Escribo t + el valor AAA de lo que quiero que se guarde (preparado para un numero hasta 256)
 *  y    ==> Veo en pantalla lo que quiero que se guarde
 *  
 *  MUCHO CUIDADO, que SDA SCL son PB7 Y PB6, algunos pinout dicen PB8 Y PB9 (no se porque este error)
 */


int a = 0;

//Variables que uso para el I2C
int num_escritura = 0;
int num_lectura = 0;

byte val = 45;
byte recibo_data = 0;

uint8 escape = 0, datos_serie = 0;
uint8 variables[4];
int contador = 0;

//Variables
int centena = 0, decena = 0, unidad = 0;

void setup() 
{
  pinMode(LED, OUTPUT);      //Coloco un led en la pata PC13

  Wire.begin();               //Libreria del I2C
  Serial.begin(9600);  
}


void loop() 
{  
  escape = 0;
  contador = 0;
  variables[3] = 0;
  
  while (Serial.available()) 
  {
    char input = Serial.read();

    switch(input) 
    {
      case 'r':
        if (a == 0)
        {
          a = 1;
          digitalWrite(LED, HIGH);
        }
        else
        {
          a = 0;
          digitalWrite(LED, LOW);
        }
        break;

      case 'y':                         //Tocando y leo lo que se escribio en la posicion de address
        Serial.print('\n');
        Serial.print("Numero leido de la memoria: ");

        Serial.println(readAddress(num_lectura), DEC);

        num_lectura = 0;
        break;
    }

    //Si envio el numero a escribir desde la compu
    while ((input == 't') && (escape == 0))
    {
      datos_serie = Serial.read();

      variables[contador] = datos_serie;
      contador++;

      //Convierto a ASCII
      centena = variables[0] - 48;
      decena = variables[1] - 48;
      unidad = variables[2] - 48;

      //Escribo por el puerto serie lo que voy a meter en la memoria
      if (variables[3] == '#')
      {
        Serial.print("Enviado: ");
        Serial.print(centena);
        Serial.print(decena);
        Serial.print(unidad);
        Serial.print('\n');

        //Meto en la memoria los datos
        val = centena * 100 + decena * 10 + unidad;
        writeAddress(num_escritura, val);

        escape = 1;
        num_escritura++;
      }
    }    
  }  
}

void writeAddress (int address, byte val)       //Funcionando para un solo bit
{
  Wire.beginTransmission(chipAddress);
  Wire.write( (int) (address >> 8));
  Wire.write( (int) (address & 0xFF));

  Wire.write(val);
  Wire.endTransmission();

  delay (20);
}

byte readAddress(int address)                   //Funcionando para un solo bit
{
  byte rData = 0;

  Wire.beginTransmission(chipAddress);
  Wire.write( (int) (address >> 8));
  Wire.write( (int) (address & 0xFF));
  Wire.endTransmission();

  Wire.requestFrom(chipAddress, 1);

  if(Wire.available())
  {
    rData = Wire.read();
  }  

  return rData;
}
